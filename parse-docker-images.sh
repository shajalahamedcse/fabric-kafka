# This scripts pulls docker images form the Dockerhub hyperledger namespace

DOCKER_NS=hyperledger
VERSION=1.3.0

# Kafka and Zookeeper
THIRDPARTY_IMAGE_VERSION=0.4.10

# set of Hyperledger Fabric images
FABRIC_IMAGES=(fabric-peer fabric-orderer fabric-ccenv fabric-tools fabric-ca)

for image in ${FABRIC_IMAGES[@]}; do
    echo "Pulling ${DOCKER_NS}/$image:${VERSION} ..."
    docker pull ${DOCKER_NS}/$image:${VERSION}
    docker tag ${DOCKER_NS}/$image:${VERSION} ${DOCKER_NS}/$image:latest
done


# TP Hyperledger Fabric images
TP_FABRIC_IMAGES=(fabric-kafka fabric-zookeeper)
for image in ${TP_FABRIC_IMAGES[@]}; do
  echo "Pulling ${DOCKER_NS}/$image:${THIRDPARTY_IMAGE_VERSION} ..."
  docker pull ${DOCKER_NS}/$image:${THIRDPARTY_IMAGE_VERSION}
  docker tag ${DOCKER_NS}/$image:${THIRDPARTY_IMAGE_VERSION} ${DOCKER_NS}/$image:latest
done

docker images ${DOCKER_NS}/*